// Бургер шапки
$(document).ready(function($) {
    let $Catalog = $('#catalog-nav');
    let $Toggle = $('.toggle');

    let defaultData = {
        maxWidth: false,
        customToggle: $Toggle,
        navTitle: 'Каталог',
        levelTitles: true,
        pushContent: '#header',
        insertClose: /*11*/ false,
        labelBack: 'Назад',
       /* labelClose: 'Закрыть',*/
        closeLevels: false
    };
    let Navigation = $Catalog.hcOffcanvasNav(defaultData);
    const update = (settings) => {
        if (Navigation.isOpen()) {
            Navigation.on('close.once', function() {
            Navigation.update(settings);
            Navigation.open();
        });

        Navigation.close();
        }
        else {
        Navigation.update(settings);
            }
        };
});

//Glide в главной
new Glide(".glide_header", {
    autoplay: false,
    type: 'carousel',
    hoverpause: false,
}).mount()
new Glide('.glide', {
    autoplay: false,
    hoverpause: false,
    type: 'carousel',
    startAt: 0,
    perView: 5,
    breakpoints: {
        600: {
            perView: 3
        }
    }
}).mount()