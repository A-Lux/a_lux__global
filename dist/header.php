<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Интернет-магазин брендовой одежды">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">		
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/glide.core.css">
	<link rel="stylesheet" href="css/glide.theme.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/hc-offcanvas-nav.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="header" id="header">
	<div class="container">
		<div class="row pt-2">
			<div class="col-xl-3">
				<img src="./images/header__logo.png" alt="Логотип">
			</div>
			<div class="col-xl-6 flexible">
				<ul class="main-nav">
					<li><a href="#">Продукция</a></li>
					<li><a href="#">О компании</a></li>
					<li><a href="#">Сертификаты</a></li>
					<li><a href="#">Контакты</a></li>
				</ul>
			</div>
			<div class="col-xl-3 flexible">
				<a href="tel:+7 700 123-60-56">+7 700 123-60-56</a>
			</div>
		</div>
		<div class="row mt-3 pb-2">
			<div class="col-xl-3">
			  <div class="burger">
				<a class="toggle">
				    <span></span>    
				</a>
				<h3>Каталог продукции</h3>
			  </div>
			</div>
			<div class="col-xl-6">
				<form action="" class="search-form">
					<input class="search-field" type="search" placeholder="Поиск по продукции">
					<input class="search-btn" type="submit" value="">
				</form>
			</div>
			<div class="col-xl-3">
				<button class="backcall" type="button">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26" height="26" viewBox="0 0 26 26">
					<image id="_001-auricular-phone-symbol-in-a-circle" data-name="001-auricular-phone-symbol-in-a-circle" width="26" height="26" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAMAAACelLz8AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAq1BMVEXkKh7////kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh7kKh4AAADTf1ywAAAAN3RSTlMAAAE6jMjwSs/yxbANp/qcOwMY0rwjiAItSQZp/sMzOFB+P6MW/UvVooDpCfeDgVjLghrcoS4xApqUXgAAAAFiS0dEOKAHpdYAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfjChMUGCdoJgXCAAABGklEQVQoz33S13LCMBAF0JV77yUYCCUQagip9///LCo2NkzG98Hj0RnLq9USqWi6YVqAZRq6xlRasR3A9XzfcwHHHlAQIoqTVLymSRwhDDrKchRl+zkxVhbIM0VBjqqmnlhdIQ8khaiInibNjRirEAqyUdQ0nWFe91QXsBlpTlTSM3gWPbEycjTSERMtBa0GxGLoZCAhWgt6GVICg0yXn2fDZfs6pNQ1yfLEwg7Y05CYZxF8sXAAjqc78tFSdgbeHkltSBf+s/eHDWUZRM2V28eJJvvVQmvLkMXzfH7xKo/fW/6cN6p4eWRpV3Sx1ZFlo2Say1nJ7Ec1SrVXJTvsBE3b9raX0mWzXv7eLmXsKkcGYGxsxobt/xH9AybhJZytVU/IAAAAAElFTkSuQmCC"/>
					</svg>
				Обратный звонок</button>

			</div>
		</div>
	</div>
	<!-- Каталог продукции -->
	<nav id="catalog-nav">
		<ul class="first-nav">
			<li class="clothes">
				<span>Одежда</span>
				<ul>
					<li class="man">
						<a href="#">Мужская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="footwear">
				<span>Обувь</span>
				<ul>
					<li class="man">
						<a href="#">Мужская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="linen">
				<span>Белье</span>
				<ul>
					<li class="man">
						<a href="#">Мужское</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женское</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li><a href="#">Большие размеры</a></li>
			<li><a href="#">Спецодежда</a></li>
			<li><a href="#">Одежда для офиса</a></li>
			<li><a href="#">Одежда для дома</a></li>
			<li><a href="#">Свадебные наряды</a></li>
			<li><a href="#">Будущие мамы</a></li>
			<li><a href="#">Все для пляжа</a></li>
			<li class="inner-nav">
				<span>Навигация</span>
				<ul>
					<li>
						<a href="#">о компании</a>
					</li>
					<li>
						<a href="#">доставка и оплата</a>
					</li>
					<li>
						<a href="#">частые вопросы</a>
					</li>
					<li>
						<a href="#">адреса магазинов</a>
					</li>
				</ul>
			</li>

		</ul>
	</nav>	
</div>

<div class="main">
	<div class="background_slider">
		<div class="glide_header">
			<div class="glide__track" data-glide-el="track">
				<ul class="glide__slides">
					<li class="glide__slide" style="background: url('images/main__banner-bg.png'); background-position: center;">
						<div class="container mt-5">
							<div class="row">
								<div class="col-xl-6">
									<h1>Упаковачная продукция</h1>
									<p>GLOBAL PACK KZ — универсальный поставщик  упаковочной продукции</p>
								</div>
								<div class="col-xl-6">
									<svg 
									 xmlns="http://www.w3.org/2000/svg"
									 xmlns:xlink="http://www.w3.org/1999/xlink"
									 width="26.599cm" height="6.421cm" class="pointers">
									<defs>
									<filter id="Filter_0">
									    <feFlood flood-color="rgb(225, 225, 225)" flood-opacity="1" result="floodOut" />
									    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									    <feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>
									<filter id="Filter_1">
									    <feFlood flood-color="rgb(225, 225, 225)" flood-opacity="1" result="floodOut" />
									    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									    <feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>
									<filter id="Filter_2">
									    <feFlood flood-color="rgb(225, 225, 225)" flood-opacity="1" result="floodOut" />
									    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									    <feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>
									<filter id="Filter_3">
									    <feFlood flood-color="rgb(225, 225, 225)" flood-opacity="1" result="floodOut" />
									    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									    <feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>
									<filter id="Filter_4">
									    <feFlood flood-color="rgb(225, 225, 225)" flood-opacity="1" result="floodOut" />
									    <feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									    <feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>

									</defs>
									<g filter="url(#Filter_0)">
									<path fill-rule="evenodd"  stroke="rgb(142, 142, 142)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
									 d="M477.500,136.000 C477.500,136.000 461.089,158.691 477.500,180.000 "/>
									</g>
									<path fill="none" stroke="rgb(142, 142, 142)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter"
									 d="M477.500,136.000 C477.500,136.000 461.089,158.691 477.500,180.000 "/>
									<a href="#">
										<text font-family="Geometria" fill="rgb(28, 28, 28)" font-weight="bold" font-size="10px" x="496.5px" y="130.318px">&#1044;&#1051;&#1071;&#32;&#1042;&#1067;&#1055;&#1045;&#1063;&#1050;&#1048;</text>
										<path fill-rule="evenodd"  fill="rgb(228, 42, 30)"
									 	d="M485.500,124.000 C487.157,124.000 488.500,125.343 488.500,127.000 C488.500,128.657 487.157,130.000 485.500,130.000 C483.843,130.000 482.500,128.657 482.500,127.000 C482.500,125.343 483.843,124.000 485.500,124.000 Z"/>
									</a>
									<g filter="url(#Filter_1)">
									<path fill-rule="evenodd"  stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
									 d="M619.500,110.000 C619.500,110.000 607.516,131.557 647.500,160.000 "/>
									</g>
									<a href="#">
										<path fill-rule="evenodd"  fill="rgb(228, 42, 30)"
										 d="M620.500,96.000 C622.157,96.000 623.500,97.343 623.500,99.000 C623.500,100.657 622.157,102.000 620.500,102.000 C618.843,102.000 617.500,100.657 617.500,99.000 C617.500,97.343 618.843,96.000 620.500,96.000 Z"/>
										
										<text font-family="Geometria" fill="rgb(28, 28, 28)" font-weight="bold" font-size="10px" x="631.5px" y="102.318px">&#1044;&#1051;&#1071;&#32;&#1043;&#1054;&#1056;&#1071;&#1063;&#1048;&#1061;&#32;&#1041;&#1051;&#1070;&#1044;</text>
									</a>
									<path fill="none" stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter"
								 	d="M619.500,110.000 C619.500,110.000 607.516,131.557 647.500,160.000 "/>
									<g filter="url(#Filter_2)">
									<path fill-rule="evenodd"  stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
									 d="M14.500,36.000 C14.500,36.000 -24.235,125.521 36.500,168.000 "/>
									</g>
									<a href="#">
										<path fill-rule="evenodd"  fill="rgb(228, 42, 30)"
										 d="M20.500,20.000 C22.157,20.000 23.500,21.343 23.500,23.000 C23.500,24.657 22.157,26.000 20.500,26.000 C18.843,26.000 17.500,24.657 17.500,23.000 C17.500,21.343 18.843,20.000 20.500,20.000 Z"/>	
										 <text font-family="Geometria" fill="rgb(28, 28, 28)" font-weight="bold" font-size="10px" x="31.5px" y="26.318px">&#1044;&#1051;&#1071;&#32;&#1050;&#1054;&#1060;&#1045;&#32;</text>
									</a>
									<path fill="none" stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter"
									 d="M14.500,36.000 C14.500,36.000 -24.235,125.521 36.500,168.000 "/>
									<g filter="url(#Filter_3)">
									<path fill-rule="evenodd"  stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
									 d="M74.500,71.000 C74.500,71.000 55.765,113.521 116.500,156.000 "/>
									</g>
									<a href="#">
										<path fill-rule="evenodd"  fill="rgb(228, 42, 30)"
										 d="M80.500,56.000 C82.157,56.000 83.500,57.343 83.500,59.000 C83.500,60.657 82.157,62.000 80.500,62.000 C78.843,62.000 77.500,60.657 77.500,59.000 C77.500,57.343 78.843,56.000 80.500,56.000 Z"/>
										<text font-family="Geometria" fill="rgb(28, 28, 28)" font-weight="bold" font-size="10px" x="91.5px" y="63.318px">&#1044;&#1051;&#1071;&#32;&#1053;&#1040;&#1055;&#1048;&#1058;&#1050;&#1054;&#1042;</text>
									</a>
									<path fill="none" stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter"
									 d="M74.500,71.000 C74.500,71.000 55.765,113.521 116.500,156.000 "/>
									<a href="#">
										<text font-family="Geometria" fill="rgb(28, 28, 28)" font-weight="bold" font-size="10px" x="448.5px" y="8.31799999999998px">&#1044;&#1051;&#1071;&#32;&#1058;&#1054;&#1056;&#1058;&#1054;&#1042;</text>
										<path fill-rule="evenodd"  fill="rgb(228, 42, 30)"
									 	d="M437.500,2.000 C439.157,2.000 440.500,3.343 440.500,5.000 C440.500,6.657 439.157,8.000 437.500,8.000 C435.843,8.000 434.500,6.657 434.500,5.000 C434.500,3.343 435.843,2.000 437.500,2.000 Z"/>
									 </a>
									<g filter="url(#Filter_4)">
									<path fill-rule="evenodd"  stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
									 d="M441.500,16.000 C441.500,16.000 467.656,61.939 427.500,107.000 "/>
									</g>
									<path fill="none" stroke="rgb(149, 149, 198)" stroke-width="1px" stroke-linecap="butt" stroke-linejoin="miter"
									 d="M441.500,16.000 C441.500,16.000 467.656,61.939 427.500,107.000 "/>
									</svg>
								</div>
							</div>
							<div class="row mt-5">
								<div class="col-xl-3">
									<a href="#" class="more">подробнее</a>
								</div>
								<div class="col-xl-3">
									<a href="#" class="instagram-link">перейти в инстаграм
										<object class="svgClass" type="image/svg+xml" data="instagram.svg">

										    <svg id="_16" data-name="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
										        <defs>
										            <style>
										                .cls-1,
										                .cls-2 {
										                    fill: #262626;
										                }

										                .cls-2 {
										                    fill-rule: evenodd;
										                }
										            </style>
										        </defs>
										        <title>footer_instagram</title>
										        
										        <path class="cls-2"
										            d="M198.42,92.41a12.68,12.68,0,1,1,12.67,12.69,12.69,12.69,0,0,1-12.67-12.69Zm25.77-24.82H198a11.77,11.77,0,0,0-11.74,11.74v26.18A11.79,11.79,0,0,0,198,117.25h26.18a11.78,11.78,0,0,0,11.74-11.74V79.33a11.76,11.76,0,0,0-11.74-11.74ZM198,70.53h26.18a8.8,8.8,0,0,1,8.8,8.8v26.18a8.81,8.81,0,0,1-8.8,8.8H198a8.81,8.81,0,0,1-8.8-8.8V79.33a8.8,8.8,0,0,1,8.8-8.8Zm28.79,3.7a2.58,2.58,0,1,0,2.58,2.57,2.57,2.57,0,0,0-2.58-2.57Zm.3,18.18a16,16,0,1,0-16,16,16,16,0,0,0,16-16Z"
										            transform="translate(-161.57 -42.89)" />
										    </svg>
										</object>
									</a>

								</div>
								<div class="col-xl-6"></div>
							</div>
						</div>
					</li>
					<li class="glide__slide" style="background: url('images/main__banner-bg.png'); background-position: center;"></li>
					<li class="glide__slide" style="background: url('images/main__banner-bg.png'); background-position: center;"></li>
					<li class="glide__slide" style="background: url('images/main__banner-bg.png'); background-position: center;"></li>
				</ul>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="glide__bullets" data-glide-el="controls[nav]">
							<button class="glide__bullet" data-glide-dir="=0"></button>
							<button class="glide__bullet" data-glide-dir="=1"></button>
							<button class="glide__bullet" data-glide-dir="=2"></button>
							<button class="glide__bullet" data-glide-dir="=3"></button>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col-xl-12">
			</div>
		</div>
	</div> -->
</div>
<div class="footer">
	
</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> 
	<script type="text/javascript" src="js/slick.min.js"></script>  
	<script src="js/hc-offcanvas-nav.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
    <script src="./js/main.js"></script>
   
</body>
</html>